public class Main {
    public static void main(String[] args) {
        System.out.println("Квадрат");
        Square square = new Square(-5, -8, 6);
        square.Perimetr();
        square.Area();
        System.out.println("Прямоугольник");
        Rectangle rectangle = new Rectangle(3, 4, 5, 10);
        rectangle.PerimetrRectangle();
        rectangle.AreaRectangle();
        rectangle.move();
        System.out.println("Круг");
        Circle circle = new Circle(6, 10, 4);
        circle.PerimetCircle();
        circle.AreaCircle();
        System.out.println("Эллипс");
        Ellipse ellipse = new Ellipse(10, 3, 4, 10);
        ellipse.PerimetrEllipse();
        ellipse.AreaEllipse();
    }
}