public class Circle extends Figures {
    private int radius;

    double perimetrCircle = 0, areaCircle = 0;
    double p = 3.14;

    public Circle(int coordinatX, int coordinatY, int radius) {
        super(coordinatX, coordinatY);
        this.radius = radius;

    }

    public int getRadius() {
        return radius;
    }

    public void PerimetCircle() {
        perimetrCircle = (2 * radius) * p;
        System.out.println("Периметр " + perimetrCircle);

    }

    public void AreaCircle() {
        areaCircle = (radius * radius) * p;
        System.out.println("Площадь " + areaCircle);
    }
}
