public class Square extends Figures {

    private int side;

    int perimetr = 0, area = 0;

    public Square(int coordinatX, int coordinatY, int side) {
        super(coordinatX, coordinatY);
        this.side = side;
    }

    public int getSide() {
        return side;
    }

    public void Perimetr() {
        perimetr = 2 * (side + side);
        System.out.println("Периметр " + perimetr);
    }

    public void Area() {
        area = side * side;
        System.out.println("Площадь " + area);
    }
}
