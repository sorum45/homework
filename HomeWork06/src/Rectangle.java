public class Rectangle extends Square {
    private int longSide;

    int perimetr = 0, area = 0;

    public Rectangle(int coordinatX, int coordinatY, int side, int longSide) {
        super(coordinatX, coordinatY, side);
        this.longSide = longSide;

    }

    public int getLongSide() {
        return longSide;
    }

    public void PerimetrRectangle() {
        perimetr = 2 * (super.getSide() + longSide);
        System.out.println("Периметр " + perimetr);

    }

    public void AreaRectangle() {
        area = longSide * super.getSide();
        System.out.println("Площадь " + area);
    }
}
