public class Ellipse extends Circle {
    private int radiusBig;

    double perimetrEllipse = 0, areaEllipse = 0;

    public Ellipse (int coordinatX, int coordinatY, int radius, int radiusBig) {
        super(coordinatX, coordinatY, radius);
        this.radiusBig = radiusBig;
    }

    public int getradiusBig() {
        return radiusBig;
    }

    public void PerimetrEllipse() {
        perimetrEllipse = ((p * super.getRadius() * radiusBig) + ((radiusBig - super.getRadius()) * 2)) / super.getRadius() + radiusBig;
        System.out.println("Периметр " + perimetrEllipse);

    }
    public void AreaEllipse() {
        areaEllipse = p * radiusBig *  super.getRadius();
        System.out.println("Площадь " + areaEllipse);
    }
}
