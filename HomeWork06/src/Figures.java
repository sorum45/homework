import java.util.Scanner;

public class Figures {
    private int coordinatX;
    private int coordinatY;

    public Figures(int coordinatX, int coordinatY) {
        this.coordinatX = coordinatX;
        this.coordinatY = coordinatY;
    }

    public int getCoordinatX() {
        return coordinatX;
    }

    public int getCoordinatY() {
        return coordinatY;
    }

    public void move() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Вводим новые координаты Х и Y");
        coordinatX = scanner.nextInt();
        coordinatY = scanner.nextInt();
    }
}

