import java.util.Arrays;

public class ArraysTasksResolver {

    public static void resolveTask(int[] array,ArrayTask task, int from, int to) {

        System.out.println(Arrays.toString(array));
        System.out.println("Интервал в массиве от " + from + " до " + to);
        int result = task.resolve(array, from, to);
        System.out.println("Результат " + result);
        }

}
