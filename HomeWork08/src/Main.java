

public class Main {
    public static void main(String[] args) {
        int[] a = {10, 20, 43, 65, 40, 92, 1234, 32, 18, 52, 77};

        ArrayTask task1 = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];

            }
            return sum;
        };
        ArrayTask task2 = (array, from, to) -> {
            int maxNumber = array[0];
            for (int i = from; i <= to; i++) {
                if (maxNumber < array[i])
                    maxNumber = array[i];
            }
            System.out.println("Максимальное число в заднном интервале " + maxNumber);
            int digit = maxNumber, sum = 0;
            while (digit > 0) {
                sum += digit % 10;
                digit /= 10;
            }


            return sum;
            };





        ArraysTasksResolver.resolveTask(a, task1, 7, 10);
        ArraysTasksResolver.resolveTask(a, task2, 0,10);
    }
}