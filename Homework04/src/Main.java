public class Main {
    public static int calcSumArrayRange (int from, int to) {
        int sum = 0;
        if (from < to){
            for (int i = from; i <=to; i++){
                sum += i;
            }
        }
        else if ( from > to){
            sum = -1;
        }
        return sum;
    }

    // функция по извелчению числа из массива, НЕ ИДЕАЛЬНА!!!!
    public static int numberArray ( int [] b){
        int toInt = 0;
        for(int j = 0; j <=b.length -1; j++){
            toInt = toInt * 10 + b[j];
        }
        return toInt;
    }
    // процедура по выводу всех четных чисел в массиве
    public static void printEvenNumbersArray ( int [] a) {

        for (int i = 0; i <=a.length -1; i++){
            if (a[i] % 2 == 0 ){
                System.out.print(a[i] + " ");

            }
        }
        //System.out.println(Arrays.toString(counts));
    }
    public static void main(String[] args) {
        int sum1 = calcSumArrayRange(1, 5);
        System.out.println("Сумма интервала: " + sum1);

        System.out.println("Четные числа в массиве: ");
        int [] x = {24, 34, 78, 23, 12, 82, 37, 65, 54, 72};
        printEvenNumbersArray(x);
        System.out.println();
        int [] y = {9, 8, 7, 2, 3};
        int toInt = numberArray(y);
        System.out.println(toInt);
    }
}