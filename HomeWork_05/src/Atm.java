public class Atm {
        int allMoney;
        int maxMoney;
        int giveMoney;
        int transaction;

        Atm(int maxMoney, int allMoney, int giveMoney) {
            this.maxMoney = maxMoney;
            this.allMoney = allMoney;
            this.giveMoney = giveMoney;
        }

        public int giveMe(int value1) {
            if (value1 < 1)
                System.out.println("Деньги не выданы");
            else if (value1 > allMoney) {
                transaction ++;
                value1 = allMoney;
                System.out.println("Недостаточно денег в банкомате, выдана сумма: " + value1);
            } else if (value1 > giveMoney) {
                transaction ++;
                value1 = giveMoney;
                System.out.println("Привышен лимит суммы выдачи. Максимальная сумма:" + giveMoney + " Выдана сумма денег:" + value1);
            } else if (value1 <= giveMoney && value1 <= allMoney) {
                transaction ++;
                allMoney -= value1;
                System.out.println("Выдана сумма: " + value1);

            }
            return value1;
        }
        public int putMoney(int value) {
            System.out.println("Какую сумму вы хотите положить на счет ?");
            if (value < 1){
                System.out.println("Деньги не внесены");
                System.out.println("Всего транзакций выполнено: " + transaction);
            }
            else if (value <= (maxMoney - allMoney)) {
                transaction ++;
                allMoney +=value;
                System.out.println("Внесена сумма: " + value);
                System.out.println("Всего транзакций выполнено: " + transaction);
            }
            else {
                transaction ++;
                int temp = 0;
                temp = ((allMoney + value) -maxMoney);
                allMoney +=value - temp;
                System.out.println("внеcена сумма: " + (value - temp));
                System.out.println("заберите лишние деньги: " + temp);
                System.out.println("Всего транзакций выполнено: " + transaction);
            }
        return value;


        }

}
