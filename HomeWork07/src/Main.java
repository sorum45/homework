import java.util.Arrays;

public class Main {
    public static void completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();

        }
    }

    public static void main(String[] args) {
        EvenNumbersPrintTask n1 = new EvenNumbersPrintTask(3, 9);
        OddNumbersPrintTask n2 = new OddNumbersPrintTask(1, 9);

        Task[] work = {n1, n2};
        completeAllTasks(work);
    }
}