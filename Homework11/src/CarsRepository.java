import java.util.List;

public interface CarsRepository {

   List<String> findColorMinMileage(String color, int mileage);
   Long findAllUniqueModel(int from, int to );
   String findMinPrice();
   double findAveragePrice(String model);





}
