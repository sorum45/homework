import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository {
    private final String fileName;
    public CarsRepositoryFileBasedImpl(String fileName) {

        this.fileName = fileName;
    }



    private static final Function<String, Car> stringCarMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        int mileage = Integer.parseInt(parts[3]);
        int price = Integer.parseInt(parts[4]);
        return new Car(number, model, color, mileage, price);

    };

    @Override //цвет и нулевой пробег
    public List<String> findColorMinMileage (String color, int mileage){
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)){
            return reader.lines()
                    .map(stringCarMapper)
                    .filter(car -> car.getColor().toLowerCase().equals(color.toLowerCase()) || car.getMileage() == mileage)
                    .map(Car::getNumber)
                    .collect(Collectors.toList());



        } catch (IOException e) {
            throw new Errors(e);
        }
    }

    @Override //Кол-во уникальных моделей
    public Long findAllUniqueModel(int from, int to) {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)){
            return reader.lines()
                    .map(stringCarMapper)
                    .filter(car -> car.getPrice() >= from && car.getPrice() <= to)
                    .map(Car::getModel)
                    .distinct()
                    .count();

        } catch (IOException e) {
            throw new Errors(e);
        }
    }
    @Override //Минимальная стоимость и цвет
    public String findMinPrice() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)){
            return reader.lines()
                    .map(stringCarMapper)
                    .min(Comparator.comparingInt(Car::getPrice))
                    .map(Car::getColor)
                    .get();



        } catch (IOException e) {
            throw new Errors(e);
        }
    }
    @Override   // Средняя стоимость
        public double findAveragePrice(String model) {
            try (Reader fileReader = new FileReader(fileName);
                 BufferedReader reader = new BufferedReader(fileReader)){
                        return reader.lines()
                        .map(stringCarMapper)
                        .filter(car -> car.getModel().toLowerCase().equals(model.toLowerCase()))
                        .mapToInt(Car::getPrice)
                        .average()
                        .getAsDouble();
            } catch (IOException e) {
                throw new Errors(e);
            }
        }




}




