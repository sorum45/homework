import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("cars.txt");

        System.out.println(carsRepository.findColorMinMileage("black",0));
        System.out.println(carsRepository.findAllUniqueModel(700000, 800000));
        System.out.println(carsRepository.findMinPrice());
        System.out.println(carsRepository.findAveragePrice("camry"));



    }
}