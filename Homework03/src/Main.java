import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int[] array = new int[scanner.nextInt()];
        System.out.println("Введите числа массива: ");
        for (int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        System.out.println("Массив чисел");
        System.out.println(Arrays.toString(array));
        int localMin = 0;
        System.out.println("Локальные минимуммы в массиве: ");
        for (int i = 0; i < array.length; ++i) {
            if (i == array.length - 1) {
                if (array[i - 1] > array[i]) {
                    System.out.print(array[i] + " ");
                    localMin += 1;
                    break;
                }
            }
            else if (i == 0) {
                if (array[i] < array[i + 1]) {
                    System.out.print(array[i] + " ");
                    localMin += 1;

                }
            }
            else if (array[i] < array[i - 1] && array[i] < array[i + 1]) {
                System.out.print(array[i] + " ");
                localMin += 1;
            }
        }
        System.out.println();
        System.out.println("Всего локальных минимумов в массиве: " + localMin);
    }
}