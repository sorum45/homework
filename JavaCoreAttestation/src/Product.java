public class Product {
    private Integer id;
    private String productName;
    private double price;

    private Integer count;

    public Product (Integer id, String productName, double price, Integer count){
        this.id = id;
        this.productName = productName;
        this.price = price;
        this.count = count;

    }

    public Integer getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public double getPrice() {
        return price;
    }

    public Integer getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", quantity=" + count +
                '}';
    }
}
