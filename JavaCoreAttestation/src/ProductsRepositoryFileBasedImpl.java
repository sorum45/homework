import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductRepostory {
    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {

        this.fileName = fileName;
    }

    private static final Function<String, Product> stringProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String productName = parts[1];
        double price = Double.parseDouble(parts[2]);
        Integer count = Integer.parseInt(parts[3]);
        return new Product(id, productName, price, count);

    };

    @Override
    public Product findById(Integer id) {
        try (Reader fileReader = new FileReader(fileName);
        BufferedReader reader = new BufferedReader(fileReader)) {
                return reader.lines()
                .map(stringProductMapper)
                .filter(product -> product.getId().equals(id))
                .findFirst()
                .orElse(null);

        } catch (IOException e) {
            throw new Errors(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)) {
                    return reader.lines()
                    .map(stringProductMapper)
                    .filter(product -> product.getProductName().toLowerCase().contains(title.toLowerCase()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new Errors(e);
        }
    }
}
