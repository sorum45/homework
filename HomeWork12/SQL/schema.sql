drop table if exists user_car;
drop table if exists ride;
drop table if exists car;

create table user_car (
                          id serial primary key,
                          first_name char (30),
                          last_name char (30),
                          phone_number bigint unique,
                          experience integer,
                          age integer check ( age >= 18 and age <= 70 ) not null,
                          drive_license bool,
                          license_category char (5),
                          rating integer check ( rating >=0 and rating <= 5 )
);

create table car (
                     id serial primary key,
                     model char (20),
                     color char(20),
                     car_number char(10) unique,
                     owner_car char(30)
);

create table ride (
                      id serial primary key,
                      id_user_car integer not null,
                      foreign key (id_user_car) references user_car(id),
                      id_car integer not null,
                      foreign key (id_car) references car(id),
                      data date,
                      start_ride time,
                      finish_ride time
);
