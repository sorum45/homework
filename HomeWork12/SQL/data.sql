insert into user_car (first_name, last_name, phone_number, experience, age, drive_license, license_category, rating)
values ('Казаев', 'Руслан', 9125213214, 13, 35, true, 'B', 4),
       ('Крохалев', 'Кирилл', 9637868184, 10, 28, true,'B', 5),
       ('Тимур', 'Закаев', 9936541257, 7, 29, true, 'B C', 4),
       ('Артур', 'Зыков', 9225724452, 12, 37, true, 'B', 5),
       ('Денис', 'Конец', 9636872525, 19, 39, true, 'B C', 5);

insert into car (model, color, car_number, owner_car)
values ('Skoda', 'black', 'Е478КХ45', 'Алексей Брюхов'),
       ('toyota', 'blue', 'О372ХУ72', 'Антон Петров'),
       ('Lada', 'grey', 'О727РТ45', 'Казаев Руслан'),
       ('Mitsubishi', 'white', 'Х669ХМ45', 'Илья Солонин'),
       ('Hyundai', 'red', 'A321ТМ45', 'Крохалев Кирилл')
;

insert into ride (id_user_car, id_car, data, start_ride, finish_ride)
values (2, 5, '2022-11-13', '09:30', '16:50'),
       (3, 4, '2022-11-23', '14:40', '19:10'),
       (1, 3, '2022-11-01', '07:00', '08:50'),
       (4, 2, '2022-10-25', '08:30', '09:50'),
       (5, 1, '2022-10-31', '07:30', '12:45');