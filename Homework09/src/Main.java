public class Main {

    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(19);
        integerList.add(23);
        integerList.add(61);
        integerList.add(47);
        integerList.add(59);
        integerList.add(61);
        integerList.add(73);
        integerList.remove(61);
        integerList.removeAt(4);
        for (int i = 0; i < integerList.size(); i++) {
            Integer integer = integerList.get(i);
            System.out.println(integer);
        }


        }
    }
